package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

public class UserDao {

  public User findByLoginInfo(String loginId, String enPassword) {
    Connection conn = null;

    try {

      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id=? and password =?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, enPassword);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _enPassword = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      return new User(id, _loginId, name, birthDate, _enPassword, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public boolean isCheckLoginId(String loginId) {

    Connection conn = null;

    try {

      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id=?";


      // selectを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // レコードを取得ない→false→登録
      if (!rs.next()) {
        return true;
      }

        // 取得できる→true→エラー

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
    return false;
  }



  public User findById(int id) {
    Connection conn = null;
    
    try {
      conn = DBManager.getConnection();
      
      String sql = "SELECT * FROM user WHERE id=?";

      PreparedStatement stmt = conn.prepareStatement(sql);
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      
      int Id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      return new User(Id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  public List<User> findAll() {

    Connection con = null;
    List<User> userList = new ArrayList<User>();

    try {

      con = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE is_admin = false";

      Statement stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public List<User> research(String loginid, String userName, String startBirthDate,
      String endBirthDate) {
    Connection con = null;
    List<User> userList = new ArrayList<User>();

    try {

      con = DBManager.getConnection();


      // 共通のSQLを宣言する
      String coSql = "SELECT * FROM user WHERE is_admin = false";

      StringBuilder stringBuilder = new StringBuilder(coSql);

      // Prepared StatementのsetStringに格納する値をリスト化
      List<String> valueList = new ArrayList<String>();
      
      // ログインID入力▶SQL生成
      if (!loginid.equals("")) {
        stringBuilder.append(" and login_id =?");
        valueList.add(loginid);
        
      }

      if (!userName.equals("")) {
        stringBuilder.append(" and name LIKE ? ");
        // System.out.println(stringBuilder.toString());
        valueList.add("%" + userName + "%");
        
      }

      if (!startBirthDate.equals("")) {
        stringBuilder.append(" and birth_date >= ?");
        System.out.println(stringBuilder.toString());
        valueList.add(startBirthDate);
        
      }

      if (!endBirthDate.equals("")) {
        stringBuilder.append(" and birth_date <= ?");
        System.out.println(stringBuilder.toString());
        valueList.add(endBirthDate);
        
      }


      PreparedStatement stmt = con.prepareStatement(stringBuilder.toString());
      //if呼ばれたら対応する変数を使う
      // index番号の取得→indexOf()
      System.out.println(stringBuilder.toString());
      for (int i = 0; i < valueList.size(); i++) {
        System.out.println(valueList.get(i));
        stmt.setString(i + 1, valueList.get(i));
   }
       
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return userList;
  }

  public void insert(String loginId, String name, Date birthDate, String password) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      
      conn = DBManager.getConnection();
      
      String sql =
          "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
      
      stmt = conn.prepareStatement(sql);

       stmt.setString(1, loginId);
       stmt.setString(2, name );
       stmt.setDate(3, birthDate);
       stmt.setString(4, password);
       
       // SQLの実行
       stmt.executeUpdate();


     } catch (SQLException e) {
      e.printStackTrace();


    }finally {
      if(conn!=null) {
        try{
          conn.close();
        }catch(SQLException e) {
          e.printStackTrace();
        }
      }
    }
      }

      public void update(int id, String loginId, String password, String name, Date birthDate) {
        Connection conn = null;
        try {

          conn = DBManager.getConnection();

          String sql =
              " UPDATE user SET password=?, name=?, birth_date=?, update_date=now() WHERE id =?";

          PreparedStatement stmt = conn.prepareStatement(sql);

          stmt.setString(1, password);
          stmt.setString(2, name);
          stmt.setDate(3, birthDate);
          stmt.setInt(4, id);

          int result = stmt.executeUpdate();
          System.out.println(result);

          stmt.close();
        } catch (SQLException e) {
          e.printStackTrace();
        } finally {
          if (conn != null) {
            try {
              conn.close();
            } catch (SQLException e) {
              e.printStackTrace();
            }
          }
        }
      }

      public void delete(int id) {
        Connection conn = null;
        try {

          conn = DBManager.getConnection();

          String sql = " DELETE FROM user WHERE id=? ";

          PreparedStatement stmt = conn.prepareStatement(sql);
          stmt.setInt(1, id);

          int result = stmt.executeUpdate();
          System.out.println(result);

          stmt.close();
        } catch (SQLException e) {
          e.printStackTrace();
        } finally {
          if (conn != null) {
            try {
              conn.close();
            } catch (SQLException e) {
              e.printStackTrace();
            }
          }
        }



      }
}

