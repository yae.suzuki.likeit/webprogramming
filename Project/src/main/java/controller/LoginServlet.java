package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public LoginServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    if (user != null) {
      response.sendRedirect("UserListServlet");
    }

    RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");

    // パスワードを受け取る▶パスワードを受け取った後にパスワードを暗号化されたものにする
    // ログイン情報を探す時に暗号化されたパスワードで探す▶

    String loginId = request.getParameter("loginid");
    String password = request.getParameter("password");

    PasswordEncoder passEncorder = new PasswordEncoder();
    String enPassword = passEncorder.encordPassword(password);

    System.out.println(enPassword);


    UserDao userDao = new UserDao();
    User findUser = userDao.findByLoginInfo(loginId, enPassword);

    if (findUser == null) {

      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
      request.setAttribute("loginId", loginId);


      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);


    }

    HttpSession session = request.getSession();
    session.setAttribute("userInfo", findUser);

    response.sendRedirect("UserListServlet");
  }
}

