package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {



      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      UserDao userDao = new UserDao();

      List<User> userList = userDao.findAll();
      request.setAttribute("userList", userList);


      // session.setAttribute("userInfo", userInfo);


      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);



    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      request.setCharacterEncoding("UTF-8");

      String loginid = request.getParameter("user-loginid");
      String userName = request.getParameter("user-name");
      String strDateStart = request.getParameter("date-start");
      String strDateEnd = request.getParameter("date-end");

      // 検索した結果複数のユーザーがヒットする可能性があるためListにする
      UserDao userDao = new UserDao();
      List<User> userList = userDao.research(loginid, userName, strDateStart, strDateEnd);

      request.setAttribute("userList", userList);
      // reauest.setAttribute("loginId",loginId);
      // request.setAttribute("name",name);
      // requset.setAttribute("startDate",startDate);
      // rewuest.setAttribute("endDate",endDate);

      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);


	}

   

}
