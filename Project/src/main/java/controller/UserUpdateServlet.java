package controller;


import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      int id = Integer.valueOf(request.getParameter("id"));
      UserDao userDao = new UserDao();
      User userInfo = userDao.findById(id);

      request.setAttribute("user", userInfo);


      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;

    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      request.setCharacterEncoding("UTF-8");

      int id = Integer.valueOf(request.getParameter("user-id"));
      String loginId = request.getParameter("loginId");
      String password = request.getParameter("password");
      String confPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String strBirthDate = request.getParameter("birth-date");
      Date birthDate = null;

      if (!strBirthDate.equals("")) {
        birthDate = Date.valueOf(strBirthDate);
      }

      if (!(password.equals(confPassword)) || loginId.equals("") || name.equals("")
          || birthDate == null) {
        // リクエストスコープに取得したパラメータをセット→パスワード以外は入力内容を出力


        User userInfo = new User(id, loginId, name, birthDate);
        request.setAttribute("user", userInfo);


        request.setAttribute("errMsg", "入力された内容は正しくありません");
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      } else {

        PasswordEncoder passEncorder = new PasswordEncoder();
        String enPassword = passEncorder.encordPassword(password);

      UserDao userDao = new UserDao();
        userDao.update(id, loginId, enPassword, name, birthDate);

      response.sendRedirect("UserListServlet");
    }

    }

}
