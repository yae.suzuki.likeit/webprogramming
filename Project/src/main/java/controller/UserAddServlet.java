package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      request.setCharacterEncoding("UTF-8");

      // 新規登録時にidが付与されるため、ここでがidは取得できない
      // int id = Integer.valueOf(request.getParameter("id"));

      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String confPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");
      String strBirthDate = request.getParameter("birth-date");

      Date birthDate = null;

      // 日付が入力されていたら▶Date型に変換代入
      if (!strBirthDate.equals("")) {
        birthDate = Date.valueOf(strBirthDate);
      }


      UserDao userDao = new UserDao();
      Boolean isCheckLoginId = userDao.isCheckLoginId(loginId);

      // Date型のequals参照
      // パスワードは必要
      if (!(password.equals(confPassword)) || loginId.equals("") || password.equals("")
          || name.equals("") || birthDate == null || !(isCheckLoginId)) {
        // 入力された値を再度リクエストスコープにいれて渡す
        User addUserInfo = new User(loginId, name, birthDate);
        request.setAttribute("user", addUserInfo);

        request.setAttribute("errMsg", "入力された内容は正しくありません");
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
      } else {

        PasswordEncoder passEncorder = new PasswordEncoder();
        String enPassword = passEncorder.encordPassword(password);

        userDao.insert(loginId, name, birthDate, enPassword);
        response.sendRedirect("UserListServlet");
      }

	}

}
